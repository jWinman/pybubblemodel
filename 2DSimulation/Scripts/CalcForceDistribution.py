import os
os.chdir("../")
import sys
sys.path.append("../libraries")

import numpy as np

import classSpheres

N = 60
phis, Z, _ = np.loadtxt("data/Output/PolyAllData/AverageZ.dat", unpack=True)
numbers = 2000
bins = np.linspace(0, 4, 100)
forceDists = np.zeros((len(phis), len(bins) - 1))
forceBins = np.zeros((len(phis), len(bins)))

for i, phi in enumerate(phis):
    print(phi)
    for number in range(numbers):
        initial = "Output/PolyAllData/Run{:d}centersN{:d}phi{:.5f}.dat".format(number, N, phi)

        mySystem = classSpheres.SoftDisks("parameter.cfg")
        mySystem.setVariable("N", N)

        mySystem.ReadInitial(initial)
        mySystem.setVariable("boxwidth_x", 1)
        mySystem.setVariable("boxwidth_y", 1)
        mySystem.setVariable("phi", phi)

        force_matrix = np.linalg.norm(mySystem.ForceMatrix(mySystem.positions), axis=2)
        force_matrix = force_matrix[force_matrix > 0]


        force_mean = np.mean(force_matrix)
        if (not np.isnan(force_mean)):
            force_matrix = force_matrix / force_mean

            forceDist, bins = np.histogram(force_matrix, bins=bins, density=True)
            forceDists[i] += forceDist / numbers
            forceBins[i] += bins / numbers

np.savetxt("data/Output/PolyAllData/DistForces.dat", forceDists.T)
np.savetxt("data/Output/PolyAllData/BinsForces.dat", forceBins.T)

import subprocess
subprocess.call("python3 PlotForceDistribution.py")
