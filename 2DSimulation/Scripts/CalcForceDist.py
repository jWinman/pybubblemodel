import os
os.chdir("../")
import sys
sys.path.append("../libraries")

import numpy as np
import scipy.optimize as sco

import classSpheres

if not os.path.exists("data/Output/PolyAllDataForces/"):
        os.makedirs("data/Output/PolyAllDataForces/")

linear = lambda x, a, b: a * x + b

N = 60

for i in range(2000):
    phis, Es = np.loadtxt("data/Output/PolyAllData/Run{:d}Energy{:d}.dat".format(i, N), unpack=True)
    phis, Es = phis[np.nonzero(Es)], Es[np.nonzero(Es)]
    (a, b), _ = sco.curve_fit(linear, phis[-10:], np.sqrt(Es)[-10:])
    phi0 = -b / a

    delta_phis = phis - phi0

    for j, (delta_phi, phi) in enumerate(zip(delta_phis, phis)):
        initial = "Output/PolyAllData/Run{:d}centersN{:d}phi{:.5f}.dat".format(i, N, phi)

        mySystem = classSpheres.SoftDisks("parameter.cfg")
        mySystem.setVariable("N", N)

        mySystem.ReadInitial(initial)
        mySystem.setVariable("boxwidth_x", 1)
        mySystem.setVariable("boxwidth_y", 1)
        mySystem.setVariable("phi", phi)

        force_matrix = np.linalg.norm(mySystem.ForceMatrix(mySystem.positions), axis=2)
        force_matrix = force_matrix[np.triu_indices(N, 1)]

        np.savetxt("data/Output/PolyAllDataForces/Forces{:d}phi{:.5f}.gz".format(i, phi), force_matrix, header="{:.5f}".format(delta_phi))
