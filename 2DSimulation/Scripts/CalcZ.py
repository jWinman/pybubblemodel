import numpy as np

directory = "PolyAllDataZs"
files = 2015 #19000

phisC = np.loadtxt("../data/Output/{}/phi0s.dat".format(directory), unpack=True)
mean_phiC = np.mean(phisC)

bins = np.arange(0.83020, 0.87, 0.0012) - mean_phiC
Z_bins = np.zeros(len(bins))
count_bins = np.zeros(len(bins))

phisAll = np.append(np.arange(0.83020, 0.86, 0.0004), np.arange(0.864, 1.20, 0.004))
ZAll = np.zeros((files, len(phisAll)))

for i in range(files):
    phiC = phisC[i]
    phis, Zs = np.loadtxt("../data/Output/{}/Run{}Z60.dat".format(directory, i), unpack=True)

    ZAll[i] = Zs

    delta_phis = phis - phiC
    Zs, delta_phis = Zs[delta_phis > 0], delta_phis[delta_phis > 0]

    for i, delta_phi in enumerate(delta_phis):
        for j, bin in enumerate(bins):
            if (delta_phi < bin):
                Z_bins[j] += Zs[i]
                count_bins[j] += 1
                break;

Z_bins /= count_bins
print(Z_bins)
bins += mean_phiC
np.savetxt("../data/Output/{}/AverageZbins.dat".format(directory), np.array([bins, Z_bins]).T)

Z_Average = np.average(ZAll, axis=0)
Z_STD = np.std(ZAll, axis=0)

np.savetxt("../data/Output/{}/AverageZ.dat".format(directory), np.array([phisAll, Z_Average, Z_STD]).T)
