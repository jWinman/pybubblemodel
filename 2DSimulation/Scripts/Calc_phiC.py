#!/usr/bin/python
import numpy as np
from scipy.optimize import curve_fit
from glob import glob
import os

def line(x, m, c):
    return m*x + c

directory="PolyAllData"
#folders = glob("../data/Output/PolyZE/*Energy*.dat")
N = 2015
phi0s = np.zeros(N)

for i in range(N):
    phi, e = np.loadtxt("../data/Output/{}/Run{}Energy60.dat".format(directory, i), unpack=True)

    fit_range = np.nonzero(e)[0]
    fit_range = fit_range[-10:]

    popt, pconv = curve_fit(line, phi[fit_range], np.sqrt(e[fit_range]))
    phi0 = -popt[1]/popt[0]
    phi0s[i] = phi0
    # print(p0)
    use_range = phi > phi0

np.savetxt("../data/Output/PolyAllDataZs/phi0s.dat".format(directory), phi0s)
print(np.mean(phi0s))
