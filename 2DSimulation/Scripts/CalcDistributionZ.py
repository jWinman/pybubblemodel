import os
os.chdir("../")
import sys
sys.path.append("../libraries")

import numpy as np

import classSpheres

N = 60
phis = np.append(np.arange(1.2, 0.86, -0.004), np.arange(0.8598, 0.83, -0.0004))
numbers = 2000
bins = 11
fracZs = np.zeros((len(phis), bins - 1))

for i, phi in enumerate(phis):
    print(phi)
    for number in range(numbers):
        initial = "Output/PolyAllData/Run{:d}centersN{:d}phi{:.5f}.dat".format(number, N, phi)

        mySystem = classSpheres.SoftDisks("parameter.cfg")
        mySystem.setVariable("N", N)

        mySystem.ReadInitial(initial)
        mySystem.setVariable("boxwidth_x", 1)
        mySystem.setVariable("boxwidth_y", 1)
        mySystem.setVariable("phi", phi)

        Zs = mySystem.calcDistZ()
        fracZ, _ = np.histogram(Zs, bins=np.arange(0, bins))
        fracZs[i] += fracZ / numbers


np.savetxt("data/DistZ.dat", fracZs.T)
