import os
os.chdir("../")
import sys
sys.path.append("../libraries")

import numpy as np
import matplotlib.pyplot as plt
import classSpheres

N = 60
phis, Zs, _ = np.loadtxt("data/Output/PolyAllData/AverageZ.dat", unpack=True)
phis, Zs = phis[::-1], Zs[::-1]

for phi, Z in zip(phis, Zs):
    initial = "Output/PolyAllData/Run0centersN{:d}phi{:.5f}.dat".format(N, phi)
    mySystem = classSpheres.SoftDisks("parameter.cfg")
    mySystem.setVariable("N", N)

    mySystem.ReadInitial(initial)
    mySystem.setVariable("boxwidth_x", 1)
    mySystem.setVariable("boxwidth_y", 1)
    mySystem.setVariable("phi", phi)

    mySystem.plot_network("Plots/PolyNetwork/centersN{:d}phi{:.5f}.png".format(N, phi), thickness=700)
