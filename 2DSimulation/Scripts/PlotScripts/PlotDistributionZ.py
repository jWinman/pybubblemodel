import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy.optimize as sco
import functools as ft

norm_fn = lambda x, s: np.exp( (-x*x)/(2.*s*s) )/(s*np.sqrt( 2.*np.pi ))

N = 60
fracZs = np.loadtxt("../../data/DistZ.dat", unpack=True) / N
phis = np.arange(0.83, 1.0, 0.005)
bins = len(fracZs[0])

_, Z, ZSTD = np.loadtxt("../../data/Output/PolyAllData/AverageZ.dat", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

colors = ["g", "maroon", "deeppink", "k", "r", "y", "b", "c", "m", "g"]
syms = ["<", "p", "*", "s", "o", "^", "v", "D", "+", "x"]
for i, sym, color in zip(range(bins - 1), syms, colors):
    ax.plot(Z, fracZs[:, i], sym, label="$n = {}$".format(i), markersize="3.", color=color)

####### fit ############
fracZscorr = np.zeros((bins, len(Z[Z > 4])))
for i in range(bins - 1):
    fracZscorr[i] = fracZs[:, i][Z > 4]

fracZs = fracZscorr.T
Z = Z[Z > 4]

sigmaInit = 0.75 # initial guess, from van Hecke
fitz = np.empty(0)
vals = np.empty(0)

for i in range(3,8):
    coords = np.isfinite(fracZs[:,i])
    fitz = np.append(fitz, Z[coords] - i)
    vals = np.append(vals, fracZs[:, i][coords])

popt, pcov = sco.curve_fit(norm_fn, fitz, vals, p0=sigmaInit)
sigma = popt[0]
sigma_error = np.sqrt(pcov[0][0])

print(sigma, sigma_error)

########################

Zlin = np.linspace(4, 6, 100)

for i, color in zip(range(3, 8), colors[3:8]):
    ax.plot(Zlin, norm_fn(i - Zlin, sigma), "--", color=color)

ax.set_xlabel("Contact number $Z$")
ax.set_ylabel( 'Fraction of bubbles with $n$ neighbours' )
ax.set_xlim(4 - 4 / N, 6)
ax.set_ylim(0, 0.7)
ax.grid()


ax.legend(loc="best", ncol=2)
fig.savefig("../../Plots/ZDist.pdf")

