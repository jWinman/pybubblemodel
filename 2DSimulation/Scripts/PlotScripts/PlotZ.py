import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

#################### PLAT #####################################
_, _, ZPLATstd, _ = np.loadtxt("../../data/Output/PLAT/z.dat", unpack=True)
phiPLAT, ZPLAT = np.loadtxt("../../data/Output/PLAT/noRattleZ.dat", unpack=True)

Linear = lambda phi, phiC, pre: pre * (phi - phiC) + 4 - 1 / 15.
phiLin = np.linspace(0.83, 1., 100000)

# Fit of PLAT
phiPLATfit = phiPLAT[np.all([phiPLAT > 0.84, phiPLAT < 0.88], axis=0)]
ZPLATfit = ZPLAT[np.all([phiPLAT > 0.84, phiPLAT < 0.88], axis=0)]
popt, pcov = sco.curve_fit(Linear, phiPLATfit, ZPLATfit, p0=[16, 0.84])
phiCPLAT = popt[0]
phiCPLAT_error = np.sqrt(pcov[0][0])
prePLAT = popt[1]
prePLAT_error = np.sqrt(pcov[1][1])

print("PLAT Linear Fit: ")
print("PhiC: ", phiCPLAT, phiCPLAT_error, "preFactor: ", prePLAT, prePLAT_error)

# log-log "wiggle" analysis
aPLAT = 1 #1.00119
bPLAT = 1.25237 #1.27842
powerLaw = lambda phi, pre, alpha: pre * (phi)**alpha
phiLog = np.logspace(-2, 1, 2000)

################ Soft disks ###################################
phisPoly, ZsPoly, ZsPolyStd = np.loadtxt("../../data/Output/PolyZE/AverageZ.dat", unpack=True)

sqrt = lambda phi, a, phi0: a * (phi - phi0)**0.5 + 4 - 1 / 15.

popt, pcov = sco.curve_fit(sqrt, phisPoly[ZsPoly > 4], ZsPoly[ZsPoly > 4], p0=[4, 0.845])
a = popt[0]
a_error = np.sqrt(pcov[0][0])
phi0 = popt[1]
phi0_error = np.sqrt(pcov[1][1])

print("Soft disks pre: ", a, a_error)
print("Soft disks phiC: ", phi0, phi0_error)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(phiLin, Linear(phiLin, phiCPLAT, prePLAT), "r", label="$Z_{{\mathrm{{lin}}}}(\phi) = Z_c + {:.1f} ({:.3f} - \phi)$".format(prePLAT, 1 - phiCPLAT))
ax.errorbar(phiPLAT[:-60], ZPLAT[:-60], yerr=ZPLATstd[:-60], fmt="r.", label="2D foam")

ax.plot(phiLin, sqrt(phiLin, a, phi0), "c", label="$Z_{{\mathrm{{sqrt}}}}(\phi) = Z_c + {:.1f} \sqrt{{{:.3f} - \phi}}$".format(a, 1 - phi0))
ax.errorbar(phisPoly, ZsPoly, yerr=ZsPolyStd, fmt="c.", label="soft disk")

handles, labels = ax.get_legend_handles_labels()
handles[1], handles[2] = handles[2], handles[1]
labels[1], labels[2] = labels[2], labels[1]

ax.set_ylabel("Average coordination number, $Z$")
ax.set_xlabel("Gas/packing fraction, $1 - \phi$")
ax.legend(handles, labels, loc="upper left", fontsize=12)

ax.set_xlim(phiCPLAT, 0.9)
ax.set_ylim(4 - 4 / 60, 5)

ZsPoly -= 4 - 4. / 59.
ZPLAT -= 4 - 4 / 60.

phiCPLAT = 1 - 0.1593087 # set PhiCPLAT to the one from wiggle plot
print("phiCPLAT: ", phiCPLAT)
phiPLAT -= phiCPLAT

# log-log fit
#phisPoly -= phi0
#phisPolyFit = np.log10(phisPoly[phisPoly > 0.01])
#ZsPolyFit = np.log10(ZsPoly[phisPoly > 0.01])
#aSD, aSD_error, bSD, bSD_error = lin.linregress(phisPolyFit, ZsPolyFit)
#print("slope: ", aSD, 10**bSD)

axInset = plt.axes([0.585, 0.24, 0.34, 0.34])
axInset.plot(phiPLAT[phiPLAT > 0], ZPLAT[phiPLAT > 0], "r.", label="2D foam")
axInset.plot(phiLog, powerLaw(phiLog, 10**bPLAT, aPLAT), "b", label="$Z - Z_c \propto (\phi_c - \phi)^{{{:.3f}}}$".format(aPLAT))
axInset.set_xlim(0.01, 0.16)
axInset.set_ylim(0.1, 2.5)
axInset.set_xscale("log")
axInset.set_yscale("log")
axInset.set_ylabel("$Z - Z_c$", labelpad=-12.)
axInset.set_xlabel("$\phi_c - \phi$", labelpad=-10.)
axInset.grid(True, which="both")
axInset.legend(loc="lower right", fontsize=10.)

fig.savefig("../../Plots/meanZPLAT.pdf")
