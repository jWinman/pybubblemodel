import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sco

directory = "PolyAllDataZs"

phiAvg, ZAvg, _ = np.loadtxt("../../data/Output/{}/AverageZ.dat".format(directory), unpack=True)
phiBins, ZBins = np.loadtxt("../../data/Output/{}/AverageZbins.dat".format(directory), unpack=True)

phiCs = np.loadtxt("../../data/Output/{}/phi0s.dat".format(directory), unpack=True)
mean_phiC = np.mean(phiCs)

sqrt = lambda phi, a, phi0: a * (phi - phi0)**0.5 + 4 - 1 / 15.
phiLin = np.linspace(0.83, 1., 1000)

popt, pcov = sco.curve_fit(sqrt, phiBins[ZBins > 4], ZBins[ZBins > 4], p0=[4, 0.841])
a = popt[0]
a_error = np.sqrt(pcov[0][0])
phi0 = popt[1]
phi0_error = np.sqrt(pcov[1][1])

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(phiBins, ZBins, ".")
ax.plot(phiAvg, ZAvg, ".")

ax.set_ylim(4 - 1 / 15., 5)
ax.set_xlim(mean_phiC, 0.9)

ax.set_ylabel("Average $Z$")
ax.set_xlabel("Packing frac $\phi$")

ax.plot(phiLin, sqrt(phiLin, a, phi0), "c", label="$Z_{{\mathrm{{sqrt}}}}(\phi) = Z_c + {:.1f} \sqrt{{\phi - {:.3f}}}$".format(a, phi0))
ax.legend()

fig.savefig("../../Plots/AverageZCompare.pdf")

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(phiAvg, ZAvg, ".")
ax.axvline(mean_phiC)

ax.set_ylim(0, 5)
ax.set_xlim(0.83, 0.9)

ax.set_ylabel("Average $Z$")
ax.set_xlabel("Packing frac $\phi$")

ax.legend()

fig.savefig("../../Plots/AverageZFiniteSize.pdf")
