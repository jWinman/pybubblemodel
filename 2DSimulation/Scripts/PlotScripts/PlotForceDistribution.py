import numpy as np
import matplotlib.pyplot as plt
import subprocess

subprocess.call("mkdir -p ../../Plots/DistForces", shell=True)

forceDists = np.loadtxt("../../data/Output/PolyAllData/DistForces.dat", unpack=True)
forceBins = np.loadtxt("../../data/Output/PolyAllData/BinsForces.dat", unpack=True)
phis, Zs, _ = np.loadtxt("../../data/Output/PolyAllData/AverageZ.dat", unpack=True)
print(np.shape(forceDists), np.shape(forceBins), np.shape(phis))
#phis = np.arange(0.8598, 0.83, -0.002)

for forceDist, forceBin, phi, Z in zip(forceDists, forceBins, phis, Zs):
    print(phi, Z)
    print(np.shape(forceDist), np.shape(forceBin))
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title(r"$\phi = {:.4f}$, $Z = {:.2f}$".format(phi, Z))

    ax.plot(forceBin[:-1], forceDist, "o")
    ax.set_xlabel("$f / <f>$")
    ax.set_ylabel("$P(f)$")
    ax.set_ylim(1e-5, 1)
    ax.set_xlim(0, 4)
    ax.set_yscale("log")

    fig.savefig("../../Plots/DistForcesLogLin/DistForces{:.4f}.png".format(phi), dpi=300)
    plt.close()
