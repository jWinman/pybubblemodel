import sys
sys.path.append("../../../libraries")

import numpy as np
import classSpheres
import os

N = 50
phis = np.arange(0.7, 1., 0.01)

os.chdir("../..")

for i, phi in enumerate(phis):
    initial = "Output/DefectStructure/centersN{:d}phi{:.5f}.dat".format(N, phi)

    mySystem = classSpheres.SoftDisks("parameter.cfg")
    mySystem.setVariable("N", N)

    mySystem.ReadInitial(initial)
    mySystem.setVariable("boxwidth_x", 1)
    mySystem.setVariable("boxwidth_y", 1)
    mySystem.setVariable("phi", phi)
    print("Phi: ", mySystem.calcPhi())
    print("Energy: ", mySystem.Energy(mySystem.positions))
    
    savefile = "Plots/DefectsNetwork/centersN{:d}phi{:.5f}.pdf".format(N, phi)
    mySystem.plot_network(savefile)
