import numpy as np
import matplotlib.pyplot as plt

N = 59
phi, E = np.loadtxt("../data/Output/DefectStructure2/Energy{}.dat".format(N), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)

ax.plot(phi, E, "o")
ax.set_ylabel("Energy $E$")
ax.set_xlabel("packing frac $\phi$")
ax.set_xlim(0.7, 1)
ax.grid()

ax = fig.add_subplot(2, 1, 2)

ax.plot(phi[:-1], np.diff(E) / np.diff(phi), "o")
ax.set_ylabel("$dE / d\phi$")
ax.set_xlabel("packing frac $\phi$")
ax.set_xlim(0.7, 1)
ax.grid()

fig.savefig("../Plots/Energy{}Defect2.pdf".format(N))
