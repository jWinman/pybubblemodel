import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time

def minimalE(N, phi, initial, finalDir, Run):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SoftDisks("parameter.cfg")
    mySystem.setVariable("N", N)
    mySystem.setVariable("phi", phi)
    mySystem.setVariable("boxwidth_x", 1.)
    mySystem.setVariable("boxwidth_y", 1.)

    if (initial == "Random"):
        import numpy.random
        import os
        random_data = os.urandom(4)
        seed = int.from_bytes(random_data, byteorder="big")
        numpy.random.seed(seed)
        mySystem.RandomInitial(0, np.sqrt(np.log(1.02)))
    else:
        mySystem.ReadInitial(initial)

#    subprocess.call("rm data/" + initial, shell=True)

    mySystem.updateRadiusPolyDisperse()
    E = mySystem.minimize("BFGS") / N
    Z, NonRattlers = mySystem.calcZ()
    Z = Z / NonRattlers if (NonRattlers > 0) else 0
    print("Energy: ", E, flush=True)
    print("Avg Z: ", Z, flush=True)

    mySystem.printToFile(finalDir + "Run{}".format(Run))

    return E, Z

def run(phis, N, Run, directory):
    finalDir = "Output/{}/".format(directory)
    E = np.zeros(len(phis))
    Z = np.zeros(len(phis))

    for i, phi in enumerate(phis):
        print(phi)
        if (i == 0):
            initial = "Random"
        else:
            initial = finalDir + "Run{}centersN{:d}phi{:.5f}.dat".format(Run, N, phis[i - 1])
        E[i], Z[i] = minimalE(N, phi, initial, finalDir, Run)

    np.savetxt("data/{}Run{}Energy{}.dat".format(finalDir, Run, N), np.array([phis, E]).T)
    np.savetxt("data/{}Run{}Z{}.dat".format(finalDir, Run, N), np.array([phis, Z]).T)

N = 120
phis = np.append(np.arange(1.2, 0.86, -0.004), np.arange(0.8598, 0.83, -0.0004))
directory = "PolyAllData120"

#[run(phis, N, Run, directory) for Run in range(10)]
from joblib import Parallel, delayed
Parallel(n_jobs=10)(delayed(run)(phis, N, Run) for Run in range(2015))
