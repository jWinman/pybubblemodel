#!/bin/bash

rm tasks?
rm taskfarming?.pbs

count=0
for j in {1..4};
do
    sed s/VARIABLE/$j/g < taskfarming.pbs > taskfarming$j.pbs;
    for i in {1..504};
    do
	x=$(cat task_raw)
	printf "$x $count\n" >> tasks$j
	let count=count+1
    done
    qsub taskfarming$j.pbs;
done
