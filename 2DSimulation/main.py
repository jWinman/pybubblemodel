import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time
from joblib import Parallel, delayed

def minimalE(N, phi, initial, finalDir):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

#    boxwidth_x = 6. / (ny - (ny % 2)) * nx / (3 * np.sqrt(3))

    mySystem = classSpheres.SoftDisks("parameter.cfg")
    mySystem.setVariable("N", N)
    mySystem.setVariable("phi", phi)
    mySystem.setVariable("boxwidth_x", 1.)
    mySystem.setVariable("boxwidth_y", 1.)

    if (initial == "Random"):
        mySystem.RandomInitialMonodisperse()
        mySystem.updateRadiusMonoDisperse()
        E = mySystem.minimize("basinhopping")
    else:
        mySystem.ReadInitial(initial)
        mySystem.updateRadiusMonoDisperse()
        E = mySystem.minimize("basinhopping")

    print("Energy: ", phi, N, E)

    mySystem.printToFile(finalDir)
#    mySystem.plot2D("")

    return E

N = int(sys.argv[1])

phis = np.arange(0.7, 1., 0.01)
finalDir = "Output/DefectStructure2/"
E = np.zeros(len(phis))

#for i, phi in enumerate(phis):
#    if (i == 0):
#        initial = "Input/Triangle/centersN{:d}phi{:.3f}.dat".format(nx * ny, phi)
#    else:
#        initial = "Output/Triangle-1/centersN{:d}phi{:.3f}.dat".format(nx * ny, phi + 0.0005)
#    E[i] = minimalE(nx, ny, phi, initial, finalDir)

for i, phi in enumerate(phis):
    if (i == 0):
        initial = "Random"
    else:
        initial = finalDir + "/centersN{:d}phi{:.5f}.dat".format(N, phis[i - 1])
    E[i] = minimalE(N, phi, initial, finalDir)

np.savetxt("data/" + finalDir + "Energy{}.dat".format(N), np.array([phis, E]).T)

#import matplotlib.pyplot as plt
#fig = plt.figure()
#ax = fig.add_subplot(1, 1, 1)
#
#ax.plot(phis, E, "o")
#
#fig.savefig("Plots/E.pdf")
