import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres

nx = 6
ny = 6

#boxwidth_x = 6. / (ny - (ny % 2)) * nx / (3 * np.sqrt(3))

N, phi = 60, 0.9
#initial = "Input/Triangle/centersN{:d}phi{:.3f}.dat".format(N, phi)
#initial = "Output/CUDAdata/VaryPhi/Pos{:.6f}.dat".format(phi)
#initial = "Output/Fritz/centersN80phi0.82000.dat"

mySystem = classSpheres.SoftDisks("parameter.cfg")
mySystem.setVariable("N", N)

#mySystem.ReadInitial(initial)
mySystem.RandomInitial(1, 0.1)
mySystem.setVariable("boxwidth_x", 1)
mySystem.setVariable("boxwidth_y", 1)
mySystem.setVariable("phi", phi)
mySystem.updateRadiusPolyDisperse()

mySystem.minimize("basinhopping")
print("Phi: ", mySystem.calcPhi())
print("Energy: ", mySystem.Energy(mySystem.positions))
print("Z: ", mySystem.calcZ())

#E = mySystem.minimize("BFGS")
#print("Phi: ", mySystem.calcPhi())
#print("Energy: ", E)
#print("Z: ", mySystem.calcZ())

mySystem.plot2D("Plots/WebsitePhi{:.3f}.png".format(phi))

#mySystem.printToFile("Output/Fritz/{}".format(sys.argv[1]))
