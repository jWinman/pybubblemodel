import numpy as np

################### Period. Boundary functions ###################
def fold(pos, L_x, L_y):
    posX = pos[0] - L_x * np.floor(pos[0] / L_x)
    posY = pos[1] - L_y * np.floor(pos[1] / L_y)

    return np.array([posX, posY])

def shift(pos, L_x, L_y):
    posX = pos[0] + L_x
    posY = pos[1] + L_y

    return np.array([posX, posY])


################### Energy functions ###################
def Harmonic(distances, diameters, selfy):
    eps = selfy.eps
    harmonicLocal= lambda r, d: 0.5 * eps * (r - d)**2
    return np.where(distances > diameters, 0, harmonicLocal(distances, diameters))

def HarmonicForce(differenceVector, diameters, selfy):
    eps = selfy.eps
    r = np.linalg.norm(differenceVector, axis=2)
    scales = np.where((r > diameters) + (r == 0), 0, eps * (r - diameters) / r)
    return scales[:, :, None] * differenceVector

def callback(x, f, accept):
    if accept:
        file_handle = open("data/Tests/EnergyRun.txt", "ab")
        print(f / 4)
        np.savetxt(file_handle, np.array([f / 4]))
        file_handle.close()


