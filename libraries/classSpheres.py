import numpy as np
import functools as ft
import scipy.spatial.distance as ssd
from scipy import optimize
import miscellaneous as misc
#import anneal

class SoftDisks:
    def __init__(selfy, fileName, interaction="softdisk", borders="periodic"):
        import sys
        sys.path.append("/ichec/home/users/jwinkelm/.local/lib/python3.4/site-packages")
        import pylibconfig2 as pylib

        with open(fileName, "r") as f:
            cfgStr = f.read()

        cfg = pylib.Config(cfgStr)

        # static parameters
        selfy.N = cfg.disks.N
        selfy.phi = cfg.disks.phi
        selfy.boxwidth_x, selfy.boxwidth_y = cfg.square.boxwidth_x, cfg.square.boxwidth_y,
        selfy.radius = np.ones(selfy.N)
        selfy.eps = cfg.disks.eps
        selfy.niter = cfg.minimize.niter
        selfy.stepsize = cfg.minimize.stepsize
        selfy.T = cfg.minimize.T

        # variable parameters
        selfy.positions = np.zeros(selfy.N * 2) # 2N position

        # energy interactions
        selfy.interaction = ft.partial(misc.Harmonic, selfy=selfy)

        # Force interactions
        selfy.interactionForce = ft.partial(misc.HarmonicForce, selfy=selfy)

    def RandomInitial(selfy, mean, sigma):
        import numpy.random
        x = numpy.random.uniform(0., selfy.boxwidth_x, selfy.N)
        y = numpy.random.uniform(0., selfy.boxwidth_y, selfy.N)
        selfy.radius = numpy.random.lognormal(mean, sigma, selfy.N)

        selfy.positions = np.array([x, y]).T.flatten()

    def RandomInitialMonodisperse(selfy):
        import numpy.random
        x = numpy.random.uniform(0., selfy.boxwidth_x, selfy.N)
        y = numpy.random.uniform(0., selfy.boxwidth_y, selfy.N)

        selfy.positions = np.array([x, y]).T.flatten()

    def ReadInitial(selfy, initial):
        x, y, r = np.loadtxt("data/" + initial, unpack=True)
        selfy.positions = np.array([x, y]).T.flatten()
        selfy.radius = r

    def ReadInitialRadii(selfy, initial):
        import numpy.random
        x = numpy.random.uniform(0., selfy.boxwidth_x, selfy.N)
        y = numpy.random.uniform(0., selfy.boxwidth_y, selfy.N)

        selfy.positions = np.array([x, y]).T.flatten()

        _, _, r = np.loadtxt("data/" + initial, unpack=True)
        selfy.radius = r

    def GhostBoundary(selfy, x):
        xs = np.reshape(x, (len(x) // 2, 2))

        xs = np.apply_along_axis(misc.fold, 1, xs, selfy.boxwidth_x, selfy.boxwidth_y)
        xAbove = np.apply_along_axis(misc.shift, 1, xs, 0, selfy.boxwidth_y)
        xAboveRight = np.apply_along_axis(misc.shift, 1, xs, selfy.boxwidth_x, selfy.boxwidth_y)
        xRight = np.apply_along_axis(misc.shift, 1, xs, selfy.boxwidth_x, 0)
        xBelowRight = np.apply_along_axis(misc.shift, 1, xs, selfy.boxwidth_x, -selfy.boxwidth_y)
        xBelow = np.apply_along_axis(misc.shift, 1, xs, 0, -selfy.boxwidth_y)
        xBelowLeft = np.apply_along_axis(misc.shift, 1, xs, -selfy.boxwidth_x, -selfy.boxwidth_y)
        xLeft = np.apply_along_axis(misc.shift, 1, xs, -selfy.boxwidth_x, 0)
        xAboveLeft = np.apply_along_axis(misc.shift, 1, xs, -selfy.boxwidth_x, selfy.boxwidth_y)
        return xs, np.concatenate((xs,
                                   xAbove, xAboveRight, xRight, xBelowRight,
                                   xBelow, xBelowLeft,  xLeft,  xAboveLeft), axis=0)

    def GhostRadius(selfy):
        radii = np.array([])
        for i in range(9):
            radii = np.append(radii, selfy.radius)

        return radii

    def Energy(selfy, x):
        x, xGhost = selfy.GhostBoundary(x)
        radiiGhost = selfy.GhostRadius()

        # Energies
        # Energies between Ghost and Real spheres
        distances = ssd.cdist(x, xGhost, metric="euclidean")
        diameters = selfy.radius[:, None] + radiiGhost[None, :]
        distances = np.delete(distances, [j + j * len(xGhost) for j in range(len(x))])
        diameters = np.delete(diameters, [j + j * len(xGhost) for j in range(len(x))])
        energy = 0.5 * np.sum(selfy.interaction(distances, diameters))

#        print("energy: ", energy)

        return energy

    def Gradient(selfy, x):
        xs, xGhost = selfy.GhostBoundary(x)

        # Gradient
        denergies = np.zeros((len(xs), 2))
        diameters = selfy.radius[:, None] + selfy.radius[None, :]
        # real-real spheres
        for i in range(9):
            xsDiffs = xs[:, None] - xGhost[None, selfy.N * i:selfy.N * (i + 1)]
            denergies += np.sum(selfy.interactionForce(xsDiffs, diameters), axis=1)

        return -denergies.flatten()

    def ForceMatrix(selfy, x):
        xs, xGhost = selfy.GhostBoundary(x)

        # Gradient
        denergies = np.zeros((len(xs), len(xs), 2))
        diameters = selfy.radius[:, None] + selfy.radius[None, :]
        # real-real spheres
        for i in range(9):
            xsDiffs = xs[:, None] - xGhost[None, selfy.N * i:selfy.N * (i + 1)]
            denergies += selfy.interactionForce(xsDiffs, diameters)

        return -denergies

    def numGradient(selfy, x, h):
        gradient = np.zeros(len(x))

        for i in range(len(gradient)):
            newpositions = np.concatenate((x[:i], np.array([x[i] + h]), x[i + 1:]))
            gradient[i] = (selfy.Energy(x) - selfy.Energy(newpositions)) / h
        return gradient

    def minimize(selfy, method):
        bnds = [(0, selfy.boxwidth_x),
                (0, selfy.boxwidth_y)] * int((len(selfy.positions)) / 2)

        if (method=="basinhopping"):
            minimizer_kwargs = {"method": "L-BFGS-B", "jac": None, "bounds": bnds}
            ret = optimize.basinhopping(selfy.Energy, selfy.positions,
                                        minimizer_kwargs=minimizer_kwargs,
                                        niter=selfy.niter, stepsize=selfy.stepsize, T=selfy.T)

        elif (method == "CG"):
            ret = optimize.minimize(selfy.Energy, selfy.positions, jac=None, method="CG")

        else:
            ret = optimize.minimize(selfy.Energy, selfy.positions, jac=None, method="L-BFGS-B", bounds=bnds)

        selfy.positions = ret.x
        return ret.fun

    def steepest_descent(selfy, stepsize, steps):
        gradnorm = np.linalg.norm(selfy.Gradient(selfy.positions))
        for i in range(steps):
            selfy.positions += stepsize * selfy.Gradient(selfy.positions)
            gradnorm = np.linalg.norm(selfy.Gradient(selfy.positions))
            print("convergence: ", gradnorm)
            if (gradnorm < 1e-10):
                break;

        return selfy.Energy(selfy.positions)


    def plot2D(selfy, path):
        import matplotlib.pyplot as plt
        pos, posGhost = selfy.GhostBoundary(selfy.positions)
        x, y = pos.T
        xGhost, yGhost = posGhost.T
        radiiGhost = selfy.GhostRadius()

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect=1)
        ax.set_ylim(0, selfy.boxwidth_y)
        ax.set_xlim(0, selfy.boxwidth_x)
        ax.plot(xGhost, yGhost, "k.")

        for i, posi in enumerate(posGhost):
            circle1 = plt.Circle(posi, radiiGhost[i], color="k", fill=False)
            ax.add_artist(circle1)

        ax.plot([0, selfy.boxwidth_x], [0, 0], "k-", linewidth=2.)
        ax.plot([0, 0], [0, selfy.boxwidth_y], "k-", linewidth=2.)
        ax.plot([0, selfy.boxwidth_y], [selfy.boxwidth_x, selfy.boxwidth_y], "k-", linewidth=2.)
        ax.plot([selfy.boxwidth_x, selfy.boxwidth_y], [selfy.boxwidth_x, 0], "k-", linewidth=2.)

        ax.set_xticks([])
        ax.set_yticks([])

#        ax.set_title('$N = {:d}; \phi = {:.3f}$'.format(selfy.N, selfy.phi))
        plt.tight_layout()
        plt.show()
#        fig.savefig(path, dpi=600)
#        plt.close()

    def setVariable(selfy, variable, value):
        exec("selfy.{} = {}".format(variable, value))

    def updateRadiusMonoDisperse(selfy):
        radius = np.sqrt(selfy.boxwidth_x * selfy.boxwidth_y * selfy.phi / (np.pi * selfy.N))
        selfy.radius = np.ones(selfy.N) * radius

    def updateRadiusPolyDisperse(selfy):
        totalArea = np.pi * np.sum(selfy.radius**2)
        scaling = np.sqrt(selfy.phi / totalArea)
        selfy.radius *= scaling

    def distance(selfy, x1,y1,x2,y2):
        """calc distance between bubble 1 and 2"""
        delta_x = x1 - x2
        delta_y = y1 - y2
        if np.fabs(delta_x) >= 0.5*selfy.boxwidth_x:
            delta_x -= np.sign(delta_x)*selfy.boxwidth_x
        if np.fabs(delta_y) >= 0.5*selfy.boxwidth_y:
            delta_y -= np.sign(delta_y)*selfy.boxwidth_y
        return np.sqrt(delta_x**2 + delta_y**2)
    # end distance

    def calcDistZ(selfy):
        contactMat = np.zeros((selfy.N, selfy.N))
        pos = np.reshape(selfy.positions, (len(selfy.positions) // 2, 2))

        for i in range(selfy.N):
            for j in range(i + 1, selfy.N):
                d_ij = selfy.distance(pos[i][0], pos[i][1], pos[j][0], pos[j][1])
                if (d_ij < selfy.radius[i] + selfy.radius[j]):
                    contactMat[i][j] = 1
                    contactMat[j][i] = 1

        contactPerDisk = np.sum(contactMat, axis=0)
        return contactPerDisk

    def calcZ(selfy):
        contactMat = np.zeros((selfy.N, selfy.N))
        pos = np.reshape(selfy.positions, (len(selfy.positions) // 2, 2))

        for i in range(selfy.N):
            for j in range(i + 1, selfy.N):
                d_ij = selfy.distance(pos[i][0], pos[i][1], pos[j][0], pos[j][1])
                if (d_ij < selfy.radius[i] + selfy.radius[j]):
                    contactMat[i][j] = 1
                    contactMat[j][i] = 1

        contactPerDisk = np.sum(contactMat, axis=0)
        NonRattlers = len(contactPerDisk[contactPerDisk > 2])

        return np.sum(contactPerDisk[contactPerDisk > 2]), NonRattlers

    def calcPhi(selfy):
        return np.pi * (np.sum(selfy.radius**2)) / (selfy.boxwidth_x * selfy.boxwidth_y)

    def boundaryMetricPlateau(selfy, x, y, L):
        radius1, radius2 = x[-1], y[-1]
        x, y = x[:-1], y[:-1]
        boundary = lambda pos: pos - L * np.floor(pos / L + 0.5)

        return ssd.norm(boundary(x - y)) - (radius1 + radius2)

    def separation(selfy, final):
        rs = np.array([selfy.positions[:selfy.N], selfy.positions[selfy.N:], selfy.radius]).T
        print(np.shape(rs))
        avg_diameter = 2 * np.mean(selfy.radius)
        distances = ssd.pdist(rs,
                            metric=ft.partial(selfy.boundaryMetricPlateau, L=selfy.boxwidth_x))

        distances /= avg_diameter
        distances = distances[distances > 0]

        np.savetxt("data/" + final + "SepN{:d}phi{:.5f}.dat".format(selfy.N, selfy.phi), distances)

    def printToFile(selfy, final):
        x, y = selfy.GhostBoundary(selfy.positions)[0].T
        Pos = np.array([x, y, selfy.radius])

        np.savetxt("data/" + final + "centersN{:d}phi{:.5f}.dat".format(selfy.N, selfy.phi), Pos.T)

    def plot_network(selfy, path, recenter=[0,0], thickness=100):
        import matplotlib.pyplot as plt

        contactMat = np.zeros((selfy.N, selfy.N))
        pos = np.reshape(selfy.positions, (len(selfy.positions) // 2, 2))

        for i in range(selfy.N):
            for j in range(i + 1, selfy.N):
                d_ij = selfy.distance(pos[i][0], pos[i][1], pos[j][0], pos[j][1])
                if (d_ij < selfy.radius[i] + selfy.radius[j]):
                    contactMat[i][j] = 1
                    contactMat[j][i] = 1

        force_matrix = selfy.ForceMatrix(selfy.positions)
        x, y = selfy.GhostBoundary(selfy.positions)[0].T
        new_x = np.copy(x) - recenter[0]
        new_y = np.copy(y) - recenter[1]

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect=1, axisbg="w")
        ax.set_xlim(0, selfy.boxwidth_x)
        ax.set_ylim(0, selfy.boxwidth_x)

        pos, posGhost = selfy.GhostBoundary(selfy.positions)
        x, y = pos.T
        xGhost, yGhost = posGhost.T
        radiiGhost = selfy.GhostRadius()

        for i, posi in enumerate(posGhost):
            circle1 = plt.Circle(posi, radiiGhost[i], color="b", fill=False)
            ax.add_artist(circle1)

        for i in range(selfy.N):
            for j in range(selfy.N):
                if (contactMat[i][j] == 1):
                    xi = new_x[i]
                    xj = new_x[j]
                    yi = new_y[i]
                    yj = new_y[j]
                    dx = 0
                    dy = 0
                    if ( np.fabs(xi - xj) > 0.5 * selfy.boxwidth_x ):
                        dx = np.sign(xi - xj)
                    if ( np.fabs(yi - yj) > 0.5 * selfy.boxwidth_y ):
                        dy = np.sign(yi - yj)
                    for boxX in [-1, 0, 1]:
                        for boxY in [-1, 0, 1]:
                            ax.plot([xi + boxX * selfy.boxwidth_x, xj + (dx + boxX) * selfy.boxwidth_x],
                                    [yi + boxY * selfy.boxwidth_y, yj + (dy + boxY) * selfy.boxwidth_y],
                                    'k-', linewidth = thickness * np.linalg.norm(force_matrix[i, j]))

        ax.plot([0, selfy.boxwidth_x], [0, 0], "k-", linewidth=2.)
        ax.plot([0, 0], [0, selfy.boxwidth_y], "k-", linewidth=2.)
        ax.plot([0, selfy.boxwidth_y], [selfy.boxwidth_x, selfy.boxwidth_y], "k-", linewidth=2.)
        ax.plot([selfy.boxwidth_x, selfy.boxwidth_y], [selfy.boxwidth_x, 0], "k-", linewidth=2.)

        ax.set_title('$N = {:d}; \phi = {:.3f}$'.format(selfy.N, selfy.phi))
        plt.tight_layout()
        fig.savefig(path, dpi=300)
        plt.close()

#        plt.show()
